#!/usr/bin/env python
#coding=utf-8

"""
Description: Normalized cross-correlation implementation in Python using OpenCV.

Author: Amaury Dehecq
Date: October 2016
"""

# Python libraries
import numpy as np
import cv2
import gdal
#from scipy.interpolate import interp1d
import argparse

# Personal libraries
import georaster as raster


def ncorr(master,slave,xGrid,yGrid,HCS,Sr,subpix=True,nover=10,progress=True):
    """
    ## Feature-tracking algorithm based on the Normalized Cross-Correlation function of OpenCV.
    Offsets are estimated on a regular grid specified by the user, for two images cropped on the same area. The output grid is a mesh from xGrid and yGrid, i.e it will have size (len(xGrid),len(yGrid)).

    Inputs:
    - master: np 2D array, master (reference) image.
    - slave: np 2D array, slave (matching) image.
    - xGrid: 1D np array, the X coordinates of the center of the matching windows
    - yGrid: 1D np array, the Y coordinates of the center of the matching windows
    - HCS: int, Half Chip Size.
    - Sr: int, Search radius.
    - Sp: int, estimate spacing.
    - subpix: bool, if True will estimate the displacement at subpixel resolution. If False, will return the integer pixel displacement, used to speed up computation.
    - nover: int, oversampling factor. The correlation is oversampled by this factor, using cubic interpolation, to estimate the subpixel displacement. If nover=10, the subpixel displacement will be estimated at 1/10 of pixel (Default is 10).
    - progress: bool, if True will display the GDAl progress bar
    Outputs:
    - xdisp/ydisp: np array, the estimated offsets along the x/y axis.
    - corr_peak: np array, the value of the correlation peak.
    - snr: np array, Signal-to-Noise Ratio, i.e the value of the correlation peak divided by the average correlation in the search window.
    - extent: [xmin, xmax, ymin, ymax] extent of the regular grid in pixel coordinates
    """

    # correlation grid size
    nr = len(yGrid)
    nc = len(xGrid)

    # prepare outputs
    xdisp = np.zeros((nr,nc))
    ydisp = np.zeros((nr,nc))
    corr_peak = np.zeros((nr,nc))
    snr = np.zeros((nr,nc))

    # check valid grid points
    ny, nx = master.shape
    xvalid = np.where((xGrid>HCS+Sr) & (xGrid<nx-HCS-Sr))[0]
    yvalid = np.where((yGrid>HCS+Sr) & (yGrid<ny-HCS-Sr))[0]
    
    # loop on all grid points
    for i in xvalid:
        if progress == True: gdal.TermProgress_nocb(float(i)/(len(xvalid)-1))
        for j in yvalid:

            # extract chip from master image
            chip = master[yGrid[j]-HCS:yGrid[j]+HCS+1,xGrid[i]-HCS:xGrid[i]+HCS+1]

            # extract search window from slave image
            ref = slave[yGrid[j]-HCS-Sr:yGrid[j]+HCS+Sr+1,xGrid[i]-HCS-Sr:xGrid[i]+HCS+Sr+1]

            # run feature-tracking 
            corr = cv2.matchTemplate(ref,chip,cv2.TM_CCORR_NORMED)

            # estimate offset of correlation peak
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(corr)
            x0 = max_loc[0]
            y0 = max_loc[1]
            corr_mean = np.mean(corr)
            
            if subpix==True:
                ## Refine locations of peak by fitting a cubic spline

                subSearch = 5;

                # subimage
                yLim = np.arange(max(0,y0-subSearch),min(y0+subSearch+1, 2*Sr+1))
                xLim = np.arange(max(0,x0-subSearch),min(x0+subSearch+1, 2*Sr+1))
                xgrid = np.expand_dims(xLim,axis=1)
                ygrid = np.expand_dims(yLim,axis=0)
                corr_sub = corr[ygrid, xgrid]
                
                # oversample ncorr by a factor nover and estimate new maximum
                ## Warning, seem to introduce a negative bias, use cv2.remap insted?
                corr_oversampl = cv2.resize(corr_sub,None,fx=nover,fy=nover,interpolation=cv2.INTER_CUBIC)
                min_valr, max_valr, min_locr, max_locr = cv2.minMaxLoc(corr_oversampl)

                xr = x0 + max_locr[0]/float(nover) - (2*subSearch+1)/2.
                yr = y0 + max_locr[1]/float(nover) - (2*subSearch+1)/2.

                ## 1D interpolation using cubic spline. Didn't work well work none symmetrical peaks. Also slowlier than the above method. Could be adapted using a 2D interpolation?
                # # find x peak, using cubic spline
                # y = np.mean(corr_sub,1)
                # f = interp1d(xLim, y, kind='cubic')
                # xx = np.arange(np.ceil(xLim[0]),np.floor(xLim[-1]),0.1)
                # yy = f(xx)
                # argmax = np.argmax(yy)
                # xr = xx[argmax]
                # # try:
                # #     xr = xx[yy == np.max(yy)]
                # # except ValueError:  # in case several max i.e conv is constant
                # #     xr=x0

                # # find y peak
                # y = np.mean(corr_sub,0)
                # f = interp1d(yLim, y, kind='cubic')
                # xx = np.arange(np.ceil(yLim[0]),np.floor(yLim[-1]),0.1)
                # yy = f(xx)
                # argmax = np.argmax(yy)
                # yr = xx[argmax]
                # # try:
                # #     yr = xx[yy == np.max(yy)]
                # # except ValueError:  # in case several max i.e conv is constant
                # #     yr=y0
            else:
                xr = x0
                yr = y0
                max_valr = max_val
                
            # convert offset to full image coordinates
            xdisp[j,i] = xr - Sr
            ydisp[j,i] = yr - Sr
            corr_peak[j,i] = max_valr
            if corr_mean!=0:
                snr[j,i] = max_valr/corr_mean
            else:
                snr[j,i] = 0
            
    return xdisp, ydisp, corr_peak, snr


def ncorr_grid(master,slave,HCS,Sr,Sp,subpix=True,nover=10,progress=True):
    """
    ## Feature-tracking algorithm based on the Normalized Cross-Correlation function of OpenCV.
    Offsets are estimated on a regular grid, for two images cropped on the same area. The grid will start at Sr + 2*HCS pixels from the edges of the images, where only full matching windows exist.
    Inputs:
    - master: np array, master (reference) image.
    - slave: np array, slave (matching) image.
    - HCS: int, Half Chip Size.
    - Sr: int, Search radius.
    - Sp: int, estimate spacing.
    - subpix: bool, if True will estimate the displacement at subpixel resolution. If False, will return the integer pixel displacement, used to speed up computation.
    - nover: int, oversampling factor. The correlation is oversampled by this factor, using cubic interpolation, to estimate the subpixel displacement. If nover=10, the subpixel displacement will be estimated at 1/10 of pixel (Default is 10).
    - progress: bool, if True will display the GDAl progress bar
    Outputs:
    - xdisp/ydisp: np array, the estimated offsets along the x/y axis.
    - corr_peak: np array, the value of the correlation peak.
    - snr: np array, Signal-to-Noise Ratio, i.e the value of the correlation peak divided by the average correlation in the search window.
    - extent: [xmin, xmax, ymin, ymax] extent of the regular grid in pixel coordinates
    """

    # create correlation grid
    # at distance < Sr+HCS the search window would be truncated
    ny, nx = master.shape
    xGrid = np.arange(Sr+HCS,nx-Sr-HCS,Sp)
    yGrid = np.arange(Sr+HCS,ny-Sr-HCS,Sp)

    xdisp, ydisp, corr_peak, snr = ncorr(master,slave,xGrid,yGrid,HCS,Sr,subpix=subpix,nover=nover,progress=progress)
            
    return xdisp, ydisp, corr_peak, snr, (xGrid[0], xGrid[-1], yGrid[0], yGrid[-1])


def load_overlap(filename1,filename2):
    """
    Load the overlapping area between two images, for use in the feature-tracking algorithm. It assumes that the 2 images have the same projection system and that the grid are just shifted by an integer number of pixels (Landsat for example). Otherwise the returned matrices might have different sizes.
    Inputs:
    - filename1/2 : str, path to the first/second image.
    Outputs:
    - img1/2 : georaster.SingleBandRaster objects containing the images extracted at the overlapping area. Return (0,0) if intersection is void.
    """

    # compute overlap
    img1 = raster.SingleBandRaster(filename1,load_data=False)
    extent = img1.intersection(filename2)
    
    # Load images in overlap
    if extent!=0:
        img1 = raster.SingleBandRaster(filename1,load_data=extent,latlon=False)
        img2 = raster.SingleBandRaster(filename2,load_data=extent,latlon=False)
        return img1, img2

    else:
        return 0, 0


if __name__=='__main__':

    
    #### Set up arguments ####

    parser = argparse.ArgumentParser(description='Compute displacement field using feature-tracking for a pair of images')

    parser.add_argument('master', type=str, help='str,path to the master image')
    parser.add_argument('slave', type=str, help='str, path to the slave image')
    parser.add_argument('HCS', type=int, help='int, half correlation chip size (chip size will be 2*HCS+1)')
    parser.add_argument('Sr', type=int, help='int, search radius')
    parser.add_argument('step', type=int, help='int, sampling step')
    parser.add_argument('outprefix', type=str, help='str, prefix of the output files (prefix_xdisp.TIF, prefix_ydisp.TIF, prefix_disp.TIF, prefix_snr.TIF)')
    parser.add_argument('--extent', dest='extent', type=str, help='extent of the grid in geographic coordinates. Default is for the overlapping zone between the two images.', nargs=4, default=None)
    parser.add_argument('-noprogress', dest='noprogress', help='Disable the progress bar',action='store_true')
    args = parser.parse_args()

    ## Read images
    img1, img2 = load_overlap(args.master, args.slave)
    #img1 = raster.SingleBandRaster(args.master)
    #img2 = raster.SingleBandRaster(args.slave)

    ## run correlation
    if args.extent==None:
        xdisp, ydisp, corr_peak, snr, extent = ncorr_grid(img1.r,img2.r,args.HCS,args.Sr,args.step,subpix=True,nover=10,progress=1-args.noprogress)
    else:
        xmin, xmax, ymin, ymax = np.float64(args.extent)
        pxll, pyll = np.int16(img1.coord_to_px(xmin,ymax,check_valid=False))
        pxur, pyur = np.int16(img1.coord_to_px(xmax,ymin,check_valid=False))
        xGrid = np.arange(pxll, pxur, args.step)
        yGrid = np.arange(pyll, pyur, args.step)

        xdisp, ydisp, corr_peak, snr = ncorr(img1.r,img2.r,xGrid,yGrid,args.HCS,args.Sr,subpix=True,nover=10,progress=1-args.noprogress)
        extent = (xGrid[0], xGrid[-1], yGrid[0], yGrid[-1])

        
    ## Compute magnitude
    disp = np.sqrt(xdisp**2+ydisp**2)

    ## Update georeferences
    gtin = img1.trans
    gtout = [ gtin[0] + extent[0]*gtin[1], gtin[1]*args.step, gtin[2], gtin[3] + extent[2] * gtin[5], gtin[4], gtin[5]*args.step]

    ## save to file
    raster.simple_write_geotiff(args.outprefix + '_xdisp.TIF', xdisp, gtout, wkt=img1.srs.ExportToWkt(), dtype=6)
    raster.simple_write_geotiff(args.outprefix + '_ydisp.TIF', ydisp, gtout, wkt=img1.srs.ExportToWkt(), dtype=6)
    raster.simple_write_geotiff(args.outprefix + '_disp.TIF', disp, gtout, wkt=img1.srs.ExportToWkt(), dtype=6)
    raster.simple_write_geotiff(args.outprefix + '_snr.TIF', snr, gtout, wkt=img1.srs.ExportToWkt(), dtype=6)
    
