#coding=utf-8

"""
Description: Set of image preprocessing tools used to improve feature-tracking.

Author: Amaury Dehecq
Date: October 2016
"""

import numpy as np
import cv2


def wallisfilt(I, W, A=0, B=0):
    """
    Wallis Filter
    Wallis generalized variance based image enhancement by adding two
    parameters to control the amount of enhancement to apply 
    
    USER INPUTS
    W : local neighbourhood pixel size [must be odd]
    A : controls the amount of enhancement (Default is 0)
    B : mean proportionality factor, i.e. weight between the global mean and the local mean (Default is 0)
    When A=0 and B=0 we get original method Wallis Operator.
    """
    # Algorithm:
    # S = standard deviation for image
    # M = mean for image
    # m = local mean for each (x,y) pixel in image NxN neighborhood
    # s = local standard deviation for each (x,y) pixel in image NxN neighborhood 

    ## valid cells
    idx = np.isfinite(I);

    # Global mean and standard deviation
    S = np.std(I[idx]);
    M = np.mean(I[idx]);

    # Local mean and standard deviation
    m=cv2.filter2D(I,-1,np.ones((W,W))/W**2)
    m2=cv2.filter2D(I**2,-1,np.ones((W,W))/W**2)
    s = np.sqrt(m2-m**2)  # computational formula for variance
    s[np.isnan(s)] = 0    # due to rounding errors, negative value can occur below the sqrt in the previous equation
    del m2
    
    # apply wallis filter
    O = S * (I - m)/(s + A) + (M * B + m*(1-B));
    
    return O
