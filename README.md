# README #

Open-source and flexible feature-tracking algorithms developed in Python using the OpenCV library.


## Summary ##

### Overall idea

Provide a set of open-source and flexible feature-tracking algorithms. The algorithms can be based on any similarity function (normalized cross-correlation, orientation correlation...), it's up to the people to implement it.
The main goal is to give as much flexibility as possible to allow e.g. :

- to run the feature-tracking on a regular or irregular grid

- to use variable matching/search window sizes

- to mask out non-interesting parts of the image

- to give an a-priori displacement field

- etc...

Secondly, the package should provide a set of preprocessing options to enhance the images prior to the feature-tracking.

Ideally, most functions could be called from within Python or as a command line using the argparse module with well-documented usage. 


### Inputs


## Package setup ##

### Python setup

The main python libraries that have been used for testing the package are listed below (installed using [miniconda](http://conda.pydata.org/miniconda.html)) :

Library	 		  Version   	      Anaconda source 
gdal                      2.0.0               np111py27_3  
libgdal                   2.0.0                         4  
matplotlib                1.5.1               np111py27_0  
numpy                     1.11.1                   py27_0  
proj4                     4.9.2                         0  
pyproj                    1.9.5.1                  py27_0  
python                    2.7.12                        1  
scipy                     0.17.1              np111py27_1  

Except for the usual libraries, this script relies on [GDAL](http://www.gdal.org/) for the manipulation of georeferenced data (GTiff etc...) and the image processing library [OpenCV](http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_tutorials.html).

### Dependencies

Other external packages are also called during the execution:

- georaster: used to manipulate georeferenced images. Can be downloaded at https://github.com/atedstone/georaster



## Running the code ##



## Contacts ##

* Repo owner or admin

Amaury Dehecq - Jet Propulsion Laboratory

amaury.dehecq@jpl.nasa.gov


## References ##